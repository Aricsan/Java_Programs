import java.util.Scanner;

public class Principal {

	public static void main(String[]arg) {

		Jugador jugador = new Jugador();//creamos un objeto de la clase jugador para poder jugar
		IA IA = new IA();//creamos un objeto de la clase I.A para poder jugar
		int opcion;//esta variable es utilizada para almacenar lo que introduzca el usuario	por teclado
		Scanner s = new Scanner(System.in);//creamos un objeto de la clase scanner para detectar lo que introduzca el usuario por teclado
		
		do {
				System.out.println();
				System.out.println("Escoge una opci�n para empezar a jugar");
				System.out.println();
				System.out.println("-----------------------");
				System.out.println("1 ----> Multijugador");
				System.out.println("2 ----> Jugar contra I.A");
				System.out.println("3 ----> Salir");
				System.out.println("-----------------------");					

				opcion = s.nextInt();//asignamos el objeto scanner a la variable que almacena la opcion elegida por el usuario

			if(opcion == 1) {
				System.out.println("Bienvenidos al Tres en Raya World Cup");
				System.out.println("----- Reglas -----");
				System.out.println("---> Cada jugador dispone de tres vidas");
				System.out.println("---> Por cada partida de juego ganada se quita una vida al oponente");
				System.out.println("---> El primero que pierda las tres vidas perder� el campeonato y su honor");
				System.out.println();
				System.out.println("----- �Bisuerte! (Suerte dos veces) -----");
				System.out.println();
			
				jugador.crear_tablero();
				System.out.print("Creando Tablero");
				jugador.animacion();								
				jugador.primer_turno();
				for(int i = 0; i < 9 & jugador.getfin() == false; i++) {
					jugador.turno();
					
					if(jugador.finalizar == true) {						
						jugador.crear_tablero();
						jugador.primer_turno();
						i = 0;
					}
					if(jugador.getempate() == 9) {						
						jugador.crear_tablero();
						jugador.primer_turno();
						i = 0;
					}
				}				
			}
			
			if(opcion == 2) {
				System.out.println("Bienvenidos al Tres en Raya Word Cup");
				System.out.println("----- Reglas -----");
				System.out.println("---> Cada jugador dispone de tres vidas");
				System.out.println("---> Por cada partida de juego ganada se quita una vida al oponente");
				System.out.println("---> El primero que pierda las tres vidas perder� el campeonato y su honor");
				System.out.println();
				System.out.println("----- �SUERTE! -----");
				System.out.println();
				
				IA.crear_tablero();
				System.out.print("Creando Tablero");
				IA.animacion();								
				IA.primer_turno();
				for(int i = 0; i < 9 & IA.getfin() == false; i++) {
					IA.turno();
					
					if(IA.finalizar == true) {
						IA.crear_tablero();
						IA.primer_turno();
						i = 0;
					}
					if(IA.getempate() == 9) {					
						IA.crear_tablero();
						IA.primer_turno();
						i = 0;
					}
				}				
			}
			
			// chiste secreto :)
			if(opcion == 7) {
				System.out.println("�Que hace una pizza en un cementerio? es que, era familiar XD");
			}
			
		}while(opcion == 1 | opcion == 2 | opcion == 7);
		System.out.println("-------------------------------------");
		System.out.println("�Hasta luego! Espero que te hayas divertido");
		System.out.println("-------------------------------------");
		s.close();
	}
}
