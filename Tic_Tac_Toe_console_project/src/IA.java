import java.util.Random;

public class IA extends Jugador{

		private int aleatorio;//variable que almacena un numero aleatorio entre 1 y 9 que utilizara la I.A para asignar e introducir la ficha en una posicion del tablero
		Random r = new Random();//objeto random para crear el numero aleatorio
		private char jugador_IA;//variable que almacena a la I.A como jugador
		private boolean turno = true;//variable para cambiar de turno
		private boolean repeticion = false;//variable para que el metodo asignacion de jugador se ejecute solo la primera vez
		private boolean repeticion2;//variable para evitar la muestra del tablero si la I.A se equivoca y asigna una ficha en una posicion invalida
		
		//Constructor
		public IA() {	
		}
		
		//asignacion de jugador con su respectiva ficha
		public void asignacion_de_jugador(){
			if(repeticion == false) {
				if(numero_aleatorio == 1) {
					jugador = 'X';
					System.out.println("Eres el jugador X");
					jugador_IA = 'O';
				}
				else {
					jugador = 'O';
					System.out.println("Eres el jugador O");
					jugador_IA = 'X';
				}
			}			
			repeticion = true;
		}
		
		//turno humano
		public void turno_humano() {
			if(turno == true) {
				if(jugador == 'X' ) {
					System.out.println("Es el turno del jugador X.");			
					meter_fichas();
					validar_ganador();
					turno = false;
				}
				
				if(jugador == 'O') {
					System.out.println("Es el turno del jugador O.");		
					meter_fichas();
					validar_ganador();
					turno = false;
				}
			}
		}
		
		//turno I.A
		public void turno_IA(){		
			turno = true;
			
				//numero aleatorio
				aleatorio = r.nextInt(9)+1;					
					
				switch(aleatorio) {
				 
					case 1:
						if(tablero[0][0] == 'X' | tablero[0][0] == 'O') {
							turno_IA();
							repeticion2 = true;							
						}
						else {
							tablero[0][0] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n superior izquierda ([0][0]) ");
							repeticion2 = false;
						}					
						break;
						
					case 2:
						if(tablero[0][1] == 'X' | tablero[0][1] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {							
							tablero[0][1] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n central superior ([0][1])");
							repeticion2 = false;
						}						
						break;
						
					case 3:
						if(tablero[0][2] == 'X' | tablero[0][2] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[0][2] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n superior derecha ([0][2]) ");
							repeticion2 = false;
						}						
						break;
						
					case 4:
						if(tablero[1][0] == 'X' | tablero[1][0] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[1][0] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n central izquierda ([1][0]) ");
							repeticion2 = false;
						}						
						break;
						
					case 5:
						if(tablero[1][1] == 'X' | tablero[1][1] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[1][1] = jugador_IA;
							System.out.println("La IA ha asignado la fica en la posici�n central ([1][1]) ");
							repeticion2 = false;
						}						
						break;
						
					case 6:
						if(tablero[1][2] == 'X' | tablero[1][2] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[1][2] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n central derecha ([1][2]) ");
							repeticion2 = false;
						}
						break;
						
					case 7:
						if(tablero[2][0] == 'X' | tablero[2][0] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[2][0] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n inferior izquierda ([2][0]) ");
							repeticion2 = false;
						}						
						break;
						
					case 8:
						if(tablero[2][1] == 'X' | tablero[2][1] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {
							tablero[2][1] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n central inferior ([2][1]) ");
							repeticion2 = false;
						}						
						break;
						
					case 9:
						if(tablero[2][2] == 'X' | tablero[2][2] == 'O') {
							turno_IA();
							repeticion2 = true;
						}
						else {							
							tablero[2][2] = jugador_IA;
							System.out.println("La IA ha asignado la ficha en la posici�n inferior derecha ([2][2]) ");
							repeticion2 = false;
						}						
						break;	
					}				
				
				for(int i = 0; i < tablero.length & repeticion2 == false; i++) {
					for(int j = 0; j < tablero[0].length; j++) {
						System.out.print(tablero[i][j]+" ");
					}
					System.out.println();
				}
				if(repeticion2 == false) {
					System.out.println();
					validar_ganador();
				}					
		}
		
		//turno completo
		public void turno() {
			asignacion_de_jugador();
			turno_humano();
			if(finalizar == false) {
			turno_IA();
			}
	}
}
