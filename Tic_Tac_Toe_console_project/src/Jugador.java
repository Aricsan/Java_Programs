import java.util.Random;

public class Jugador extends Tablero{

	private char fichax = 'X';//variable para crear y/o almacenar la ficha x
	private char fichao = 'O';//variable para crear y/o almacenar la ficha o
	private boolean turno = true;//variable para cambiar de turno
	protected int numero_aleatorio;//variable que almacena un numero aleatorio entre 0 y 1 para saber que jugador ejecuta el primer turno
	Random r = new Random();//objeto de la clase random que crea el numero aleatorio
	
	//Constructor
	public Jugador() {	
	}
	
	//primer turno
	public void primer_turno(){
	numero_aleatorio = r.nextInt(2);
	if(numero_aleatorio == 0) {
			turno = false;
		}
	finalizar = false;
	empate = 0;
	}	
	
	//turno
	public void turno() {
		if(finalizar == false) {
			if(turno == true) {
				System.out.println("Es el turno del jugador X.");
				jugador = fichax;			
				meter_fichas();
				validar_ganador();
				turno = false;
			}
			else {
				System.out.println("Es el turno del jugador O.");
				jugador = fichao;			
				meter_fichas();
				validar_ganador();
				turno = true;
			}
		}
		
	}	
}
