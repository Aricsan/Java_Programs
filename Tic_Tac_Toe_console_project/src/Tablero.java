import java.util.Scanner;

public class Tablero{

	protected char [][] tablero = new char [3][3];//declaramos y creamos una matriz de 3x3 que sera el tablero
	Scanner s = new Scanner(System.in);//scanner para "saber" donde asigna el jugador la ficha, es decir, la posicion
	protected int opcion;//variable para almacenar la opcion del jugador que asigna la ficha en una posicion
	protected char jugador;//variable que almacena la ficha que sera introducida en el tablero por cada jugador
	protected int empate;//variable que se incrementa cada turno por un posible empate
	protected boolean finalizar = false;//variable booleana para finalizar una partida
	private int vidasx;//variable para almacenar las vidas del jugador x
	private int vidaso;//variable para almacenar las vidas del jugador o
	private boolean repeticion;//variable para evitar la muestra del tablero si un jugador se equivoca y asigna una ficha en una posicion invalida
	private boolean fin = false;//variable para poner fin al juego
	private boolean ganador;//variable que identifica al ganador
			
	//Constructor
	public Tablero() {
	}
	
	//metodo getempate
	public int getempate() {
		return empate;
	}
	
	//metodo getfin
	public boolean getfin() {
		return fin;
	}
	
	//Creacion del tablero
	public void crear_tablero() {
		for(int i = 0; i < tablero.length; i++) {
			for(int j = 0; j < tablero[0].length; j++) {
				tablero[i][j]='.';
			}			
		}
	}
	
	//Animacion de reinicio
	public void animacion() {
		for(int i = 0; i < 3; i++) {
			try {
				Thread.sleep(1000);
			}catch(InterruptedException ex) {
				System.out.println("Error 0X8932742");
			}
			System.out.print(".");
		}
		System.out.println();
	}
	
	//vidas
	public void vidas() {
		//Vidas del jugador O
		if(ganador == false) {
			vidaso++;
			if(vidaso == 1) {
				System.out.println("El ganador es el jugador O y ha destru�do una vida del jugador X");
				System.out.println("(\\/) (\\/)");
				System.out.println(" \\/   \\/" );		
				System.out.print("Vaciando Tablero" );
				animacion();
			}
			else {
				if(vidaso == 2) {
					System.out.println("El ganador es el jugador O y ha destru�do una vida del jugador X");
					System.out.println("(\\/)");
					System.out.println(" \\/" );
					System.out.print("Vaciando Tablero" );
					animacion();
				}
				else {
					if(vidaso == 3) {
						System.out.println("Damas y Caballeros, el ganador del vigesimosegundo campeonato del mundo del juego Tres en Raya, es el jugador O");
						System.out.println("Se le har� entrega del Trofeo en un plis plas");
						System.out.print("\"Comprando un Trofeo para el ganador\"");
						animacion();
						System.out.println("Enhorabuena");
						System.out.println(" ---------");
						System.out.println("|   ___   |");
						System.out.println("| (| $ |) |");
						System.out.println("|   \\ /   |");
						System.out.println("|    *    |");
						System.out.println("|   ###   |");
						System.out.println(" ---------");
						vidaso = 0;
						System.out.print("Reiniciando Juego");
						animacion();
						fin = true;
					}				
				}
			}
		}
		else {
			//Vidas del jugador X
			vidasx++;
			if(vidasx == 1) {
				System.out.println("El ganador es el jugador X y ha destru�do una vida del jugador O");
				System.out.println("(\\/) (\\/)");
				System.out.println(" \\/   \\/" );			
				System.out.print("Vaciando Tablero" );
				animacion();
			}
			else {
				if(vidasx == 2) {
					System.out.println("El ganador es el jugador X y ha destru�do una vida del jugador O");
					System.out.println("(\\/)");
					System.out.println(" \\/" );
					System.out.print("Vaciando Tablero" );
					animacion();
				}
				else {
					if(vidasx == 3) {
						System.out.println("Damas y Caballeros el ganador del vigesimosegundo campeonato del mundo del juego Tres en Raya es el jugador X");
						System.out.println("Se le hara entrega del Trofeo en un plis plas");
						System.out.print("\"Comprando un Trofeo para el ganador\"");
						animacion();
						System.out.println("Enhorabuena");
						System.out.println(" ---------");
						System.out.println("|   ___   |");
						System.out.println("| (| $ |) |");
						System.out.println("|   \\ /   |");
						System.out.println("|    *    |");
						System.out.println("|   ###   |");
						System.out.println(" ---------");
						vidasx = 0;
						System.out.print("Reiniciando Juego");
						animacion();
						fin = true;
					}				
				}
			}
		}		
	}
			
	//Asignacion de Fichas
	public void meter_fichas() {
		
			System.out.println("Elige una de las posiciones para introducir la ficha ");
			System.out.println();
			System.out.println("Pulse 1 para introducir la ficha en la posici�n superior izquierda del tablero");
			System.out.println("Pulse 2 para introducir la ficha en la posici�n central superior del tablero");
			System.out.println("Pulse 3 para introducir la ficha en la posici�n superior derecha del tablero");
			System.out.println("Pulse 4 para introducir la ficha en la posici�n central izquierda del tablero");
			System.out.println("Pulse 5 para introducir la ficha en la posici�n central del tablero");
			System.out.println("Pulse 6 para introducir la ficha en la posici�n central derecha del tablero");
			System.out.println("Pulse 7 para introducir la ficha en la posici�n inferior izquierda del tablero");
			System.out.println("Pulse 8 para introducir la ficha en la posici�n central inferior del tablero");
			System.out.println("Pulse 9 para introducir la ficha en la posici�n inferior derecha del tablero");
			System.out.println();
			
			opcion = s.nextInt();
				
				switch(opcion) {
					case 1:
						if(tablero[0][0] == 'X' | tablero[0][0] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");						
							meter_fichas();
							repeticion = true;							
						}
						else {							
							System.out.println("Has elegido la opci�n 1, la ficha se ha asignado en la posici�n superior izquierda ([0][0]) ");
							tablero[0][0] = jugador;
							repeticion = false;
						}					
						break;
						
					case 2:
						if(tablero[0][1] == 'X' | tablero[0][1] == 'O') {
							System.out.println("Movimiento err�neo. La posicion ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {							
							System.out.println("Has elegido la opci�n 2, la ficha se ha asignado en la posici�n central superior ([0][1]) ");
							tablero[0][1] = jugador;
							repeticion = false;
						}						
						break;
						
					case 3:
						if(tablero[0][2] == 'X' | tablero[0][2] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 3, la ficha se ha asignado en la posici�n superior derecha ([0][2]) ");
							tablero[0][2] = jugador;
							repeticion = false;
						}						
						break;
						
					case 4:
						if(tablero[1][0] == 'X' | tablero[1][0] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 4, la ficha se ha asignado en la posici�n central izquierda ([1][0]) ");
							tablero[1][0] = jugador;
							repeticion = false;
						}						
						break;
						
					case 5:
						if(tablero[1][1] == 'X' | tablero[1][1] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 5, la ficha se ha asignado en la posici�n central ([1][1]) ");
							tablero[1][1] = jugador;
							repeticion = false;
						}						
						break;
						
					case 6:
						if(tablero[1][2] == 'X' | tablero[1][2] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 6, la ficha se ha asignado en la posici�n central derecha ([1][2]) ");
							tablero[1][2] = jugador;
							repeticion = false;
						}
						break;
						
					case 7:
						if(tablero[2][0] == 'X' | tablero[2][0] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 7, la ficha se ha asignado en la posici�n inferior izquierda ([2][0]) ");
							tablero[2][0] = jugador;
							repeticion = false;
						}						
						break;
						
					case 8:
						if(tablero[2][1] == 'X' | tablero[2][1] == 'O') {
							System.out.println("Movimiento erroneo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 8, la ficha se ha asignado en la posici�n central inferior ([2][1]) ");
							tablero[2][1] = jugador;
							repeticion = false;
						}						
						break;
						
					case 9:
						if(tablero[2][2] == 'X' | tablero[2][2] == 'O') {
							System.out.println("Movimiento err�neo. La posici�n ya esta asignada");
							meter_fichas();
							repeticion = true;
						}
						else {
							System.out.println("Has elegido la opci�n 9, la ficha se ha asignado en la posici�n inferior derecha ([2][2]) ");
							tablero[2][2] = jugador;
							repeticion = false;
						}						
						break;
						
					default:
						System.out.println("Has elegido una posici�n inexistente, �No hagas trampas! ");
						meter_fichas();
						repeticion = true;
				}

		for(int i = 0; i < tablero.length & repeticion == false; i++) {
			for(int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	//Validar ganador
	public void validar_ganador() {
		
		//Comprobacion del jugador X
			for(int i = 0; i < 1; i++) {
				empate++;
				//Ganar en diagonal
					if(tablero[0][0] == 'X' & tablero[1][1] == 'X' & tablero[2][2] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
					if(tablero[0][2] == 'X' & tablero[1][1] == 'X' & tablero[2][0] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				
				//Ganar en perperdicular
					if(tablero[0][1] == 'X' & tablero[1][1] == 'X' & tablero[2][1] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
					if(tablero[1][0] == 'X' & tablero[1][1] == 'X' & tablero[1][2] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				
				//ganar en linea (arriba)
					if(tablero[0][0] == 'X' & tablero[0][1] == 'X' & tablero[0][2] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				//(abajo)
					if(tablero[2][0] == 'X' & tablero[2][1] == 'X' & tablero[2][2] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				//(izquierda)
					if(tablero[0][0] == 'X' & tablero[1][0] == 'X' & tablero[2][0] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				//(Derecha)
					if(tablero[0][2] == 'X' & tablero[1][2] == 'X' & tablero[2][2] == 'X' ) {						
					finalizar = true;
					ganador = true;
					vidas();
						break;
					}
				
		//Comprobacion jugador O
					
				//Ganar en diagonal
					if(tablero[0][0] == 'O' & tablero[1][1] == 'O' & tablero[2][2] == 'O' ) {
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
					if(tablero[0][2] == 'O' & tablero[1][1] == 'O' & tablero[2][0] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
					
				//Ganar en perperdicular
					if(tablero[0][1] == 'O' & tablero[1][1] == 'O' & tablero[2][1] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
					if(tablero[1][0] == 'O' & tablero[1][1] == 'O' & tablero[1][2] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
					
				//ganar en linea (arriba)
					if(tablero[0][0] == 'O' & tablero[0][1] == 'O' & tablero[0][2] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
				//(abajo)
					if(tablero[2][0] == 'O' & tablero[2][1] == 'O' & tablero[2][2] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
				//(izquierda)
					if(tablero[0][0] == 'O' & tablero[1][0] == 'O' & tablero[2][0] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
				//(Derecha)
					if(tablero[0][2] == 'O' & tablero[1][2] == 'O' & tablero[2][2] == 'O' ) {						
						finalizar = true;
						ganador = false;
						vidas();
						break;
					}
					
		//Comprobacion de Empate
					
					if(empate == 9){
						System.out.println("Ha habido un empate, volvamos a intentarlo");
						System.out.print("Vaciando Tablero");
						animacion();
					}
			}
	}
}
